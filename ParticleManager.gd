extends Node

var particles = {}
export (Array, NodePath) var nodes = []

func _ready():
	print(nodes.size())
	for node in nodes:
		var currentNode = get_node(node)
		particles[currentNode.name] = currentNode
		

func playParticle(particleName, startPlaying = true):
	if(particles.has(particleName)):
		if(startPlaying):
			particles[particleName].emitting=true
		else:
			particles[particleName].emitting=false
		if(particleName=="Stars1"):
			particles["Smoke1"].subtractSmoke()
		if(particleName=="Stars2"):
			particles["Smoke2"].subtractSmoke()
			

