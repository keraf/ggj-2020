extends VBoxContainer

export var CAR_ID = 0

const cars = {
	0: preload("res://assets/Icons/Icon_Car.png"),
	1: preload("res://assets/Icons/Icon_CarRed.png"),
}

func _ready():
	$TextureRect.texture = cars[CAR_ID]
	pass
