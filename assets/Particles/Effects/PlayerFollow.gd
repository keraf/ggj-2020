extends Node


var playerPosition
var offset
export (NodePath) var player 


# Called when the node enters the scene tree for the first time.
func _ready():
	playerPosition =  get_node(player).translation
	offset = self.transform.origin - playerPosition


func _process(delta):
	playerPosition =  get_node(player).translation
	self.translation =playerPosition + offset

