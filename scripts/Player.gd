extends KinematicBody

export var PLAYER_ID = 0
export var SPEED = 15

signal interact
signal walk_start
signal walk_stop
signal pickup_tool
signal drop_tool
signal repair_start
signal repair_end
signal open_toolbox

const input_map = {
	0: {
		"game_forward": "p1_game_forward",
		"game_backward": "p1_game_backward",
		"game_left": "p1_game_left",
		"game_right": "p1_game_right",
		"game_item_use": "p1_game_item_use",
		"game_item_drop": "p1_game_item_pick_drop"
	},
	1: {
		"game_forward": "p2_game_forward",
		"game_backward": "p2_game_backward",
		"game_left": "p2_game_left",
		"game_right": "p2_game_right",
		"game_item_use": "p2_game_item_use",
		"game_item_drop": "p2_game_item_pick_drop"
	}
}

var velocity = Vector3(0, 0, 0)
var _old_velocity = velocity
var _was_interacting = false
var nearby_tool = null
var holding_tool = null
var can_interact = true
var can_move = false
var is_near_toolbox = false

func _ready():
	pass
	
func _physics_process(delta):
	if can_move:
		var is_interacting = interaction_loop(delta)
		move_loop(is_interacting)
		item_loop()

func move_loop(is_interacting):
	if not is_interacting:
		velocity.x = get_action_strength("game_right", "game_left")
		velocity.z = get_action_strength("game_backward", "game_forward")
		velocity *= SPEED
	else:
		velocity = Vector3.ZERO
		
	_update_rotation()
	move_and_slide(velocity)
	_check_walk_signals()
	
	_old_velocity = velocity

func _update_rotation():
	if velocity.length_squared() > 0:
		self.rotation = Vector3(0, atan2(velocity.x, velocity.z), 0)
		
func _check_walk_signals():
	var old_speed = _old_velocity.length_squared()
	var new_speed = velocity.length_squared()
	if old_speed == 0 && new_speed != 0:
		emit_signal("walk_start")
		#print("walk start")
	elif old_speed != 0 && new_speed == 0:
		emit_signal("walk_stop")
		#print("walk stop")

func item_loop():
	# Picking and dropping
	if is_input_released("game_item_use") and is_tool_nearby() and not is_holding_tool():
		if pickup(nearby_tool):
			nearby_tool = null
	elif is_holding_tool() and is_input_released("game_item_drop"):
		drop()
		
	# Carrying
	if is_holding_tool():
		holding_tool.set_global_transform($RightHand.get_global_transform())

func pickup(tool_obj):
	if can_interact:
		pickup_drop_cooldown()
		holding_tool = tool_obj
		print("Picked up %s" % holding_tool.name)
		emit_signal("pickup_tool")
		print("pickup tool")
		return true
	else:
		return false
	
func drop():
	if can_interact:
		pickup_drop_cooldown()
		nearby_tool = holding_tool
		holding_tool = null
		emit_signal("drop_tool")
		print("drop tool")

func is_input_pressed(input):
	return Input.is_action_pressed(input_map[PLAYER_ID][input])
	
func is_input_released(input):
	return Input.is_action_just_pressed(input_map[PLAYER_ID][input])
	
func get_action_strength(input_pos, input_neg):
	return Input.get_action_strength("p%d_%s" % [PLAYER_ID+1, input_pos]) \
		- Input.get_action_strength("p%d_%s" % [PLAYER_ID+1, input_neg])

func is_holding_tool():
	return is_instance_valid(holding_tool) 

func is_tool_nearby():
	return is_instance_valid(nearby_tool)

func pickup_drop_cooldown():
	can_interact = false
	$PickupDropCooldown.start()

func _on_PickupDropCooldown_timeout():
	can_interact = true

func interaction_loop(delta):
	if is_near_toolbox and is_input_released("game_item_use"):
		#if is_holding_tool():
		#	holding_tool.queue_free()
		#	holding_tool = null

		emit_signal("open_toolbox", PLAYER_ID)
	elif can_interact && is_input_pressed("game_item_use") && is_holding_tool():
		# currently is interacting
		if not _was_interacting:
			emit_signal("repair_start")
			#print("start repair")
		emit_signal("interact", holding_tool, delta)
		_was_interacting = true
	else:
		# currently not interacting
		if _was_interacting:
			emit_signal("repair_end")
			#print("stop repair")
		_was_interacting = false
	return _was_interacting

func _on_Level_start():
	can_move = true
	is_near_toolbox = false

func create_tool(tool_ref):
	print("creating " + tool_ref.name)
	var new_tool = tool_ref.duplicate()
	get_parent().add_child(new_tool)
	pickup(new_tool)

func _on_Level_end(isWin, time):
	can_move = false

func _on_ToolboxArea_area_entered(area):
	is_near_toolbox = true

func _on_ToolboxArea_area_exited(area):
	is_near_toolbox = false

func _on_Player_freeze_toggle(player_id, freeze):
	if player_id == PLAYER_ID:
		can_move = not freeze
