extends Area

export(String) var skill_required

signal repair_finished
signal on_repair

export var part_id = 0
export var repair_time = 1.0
export(Array, STATIC_TYPES.TOOLS_TYPE) var tools_needed 
export(Array, int, 0) var repaired_total

var _currently_repairing = 0

const REPAIRED_MAX = 100
const REPAIR_FACTOR = 1 # use higher values during dev to speed things up

func _on_interactable_entered(body):
	if body.is_in_group("players"):
		#print("player entered " + self.name)
		body.connect("interact", self, "on_player_repair")

func _on_interactable_exited(body):
	if body.is_in_group("players"):
		#print("player exited " + self.name)
		body.disconnect("interact", self, "on_player_repair")

func on_player_repair(used_tool, delta_time):
	if not _needed_tool_is_valid():
		return
		
	if used_tool.tool_type != _current_needed_tool():
		print("Using wrong tool. Need %s" % STATIC_TYPES.get_tool_name(_current_needed_tool()))
		return

	var repaired_prev = repaired_total[_currently_repairing]
	var total_repair_time = repair_time * used_tool.repair_time
	var final_repair = REPAIRED_MAX / total_repair_time * delta_time
	
	repaired_total[_currently_repairing] = clamp(
		repaired_total[_currently_repairing] + final_repair * REPAIR_FACTOR, 
		0, REPAIRED_MAX)
	
	emit_signal("on_repair", part_id, _currently_repairing, repaired_total[_currently_repairing])
	
	print("repairing %s with %s to %f" % [
		self.name,
		STATIC_TYPES.get_tool_name(_current_needed_tool()),
		repaired_total[_currently_repairing],
	])
		
	if repaired_total[_currently_repairing] != repaired_prev \
			&& repaired_total[_currently_repairing] == REPAIRED_MAX:
		_finished_current_repair()
	
func _finished_current_repair():
	_currently_repairing += 1
	if not _needed_tool_is_valid():
		# part repaired with all tools, send signal
		emit_signal("repair_finished")
	
func _current_needed_tool():
		return tools_needed[_currently_repairing]
		
func _needed_tool_is_valid():
	return _currently_repairing < len(repaired_total)
