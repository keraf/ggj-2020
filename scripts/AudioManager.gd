extends Node

var sounds = {}
export (Array, NodePath) var nodes = []
var soundtrackName = "Soundtrack"

func _ready():
	print(nodes.size())
	for node in nodes:
		var currentNode = get_node(node)
		sounds[currentNode.name] = currentNode

func playSound(soundName, startPlaying = true):
	if(sounds.has(soundName)):
		sounds[soundName].stop()
		if(startPlaying):
			sounds[soundName].play(0)
