extends Spatial

signal update_timer
signal start
signal end
signal freeze_player
signal timer_start

export var time_left = 300
var countdown = 3

var _cars_repaired_count = 0
var can_use_toolbox = {
	0: true,
	1: true,
}

func _ready():
	pass

func _on_StartTimer_timeout():
	if countdown==3:
		emit_signal("timer_start")
	countdown -= 1
	if countdown == 0:
		$CountDown/Label.text = "Go!"
	elif countdown < 0:
		$CountDown/Label.visible = false
		$HUD.visible = true
		$StartTimer.stop()
		$GameTimer.start()
		emit_signal("start")
	else:
		$CountDown/Label.text = "%d" % countdown

func _on_GameTimer_timeout():
	time_left -= 1
	emit_signal("update_timer", time_left)
	
	if time_left <= 0:
		_game_over()

func end_game(isWin):
	$GameTimer.stop()
	$EndScreen.visible = true
	$HUD.visible = false
	# Time is hardcoded here
	emit_signal("end", isWin, (120 - time_left))

func _game_over():
	end_game(false)
	
func _on_game_win():
	end_game(true)

func _on_car_repaired():
	_cars_repaired_count += 1
	if _cars_repaired_count >= 2:
		_on_game_win()

func _on_EndScreen_restart():
	get_tree().reload_current_scene()

func playSound(extra_arg_0):
	pass # Replace with function body.

func _on_open_toolbox(player_id):
	toggle_toolbox(player_id, true)
	
func toggle_toolbox(player_id, is_visible):
	if not can_use_toolbox[player_id]:
		return
	
	emit_signal("freeze_player", player_id, is_visible)
	
	if not is_visible:
		can_use_toolbox[player_id] = false
	
	if player_id == 0:
		$Toolboxes/HBoxContainer/ToolboxOneContainer/ToolboxOne.visible = is_visible
		if not is_visible:
			$Toolboxes/HBoxContainer/ToolboxOneContainer/Cooldown.start()
	else:
		$Toolboxes/HBoxContainer/ToolboxTwoContainer/ToolboxTwo.visible = is_visible
		if not is_visible:
			$Toolboxes/HBoxContainer/ToolboxTwoContainer/Cooldown.start()

func _on_tool_created(player_id, tool_enum):
	var player = self.find_node("Player%d" % player_id, false)
	var tool_path_name = "Tools/Player%d/%s" % \
		[player_id, STATIC_TYPES.tool_scene_paths[tool_enum]]
	var tool_ref = get_node(tool_path_name)
	player.create_tool(tool_ref)
	toggle_toolbox(player_id, false)
	print("spawning tool %s on player %d" % [STATIC_TYPES.get_tool_name(tool_enum), player_id])

func _on_Cooldown_timeout(player_id):
	can_use_toolbox[player_id] = true
