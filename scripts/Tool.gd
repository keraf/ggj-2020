extends RigidBody

export(STATIC_TYPES.TOOLS_TYPE) var tool_type
export(float) var repair_time = 1.0
export(bool) var tool_is_better = false

func _ready():
	pass

func _on_Area_body_entered(body):
	if body.is_in_group("players"):
		body.nearby_tool = self

func _on_Area_body_exited(body):
	if body.is_in_group("players"):
		body.nearby_tool = null
