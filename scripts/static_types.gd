extends Node

class_name STATIC_TYPES

enum TOOLS_TYPE {
	Wrench,
	Crick,
	Hammer,
	ScrewDriver,
	BlowTorch,
	CrossKey
}

enum CAR_PARTS {
	Wheel,
	Motor,
	Exhaust,
}

static func get_tool_name(i):
	return TOOLS_TYPE.keys()[i]

static func get_part_name(i):
	return CAR_PARTS.keys()[i]
	
const tool_scene_paths = {
	TOOLS_TYPE.Wrench: "Wrench",
	TOOLS_TYPE.Crick: "Crick",
	TOOLS_TYPE.Hammer: "Hammer",
	TOOLS_TYPE.ScrewDriver: "Screwdriver",
	TOOLS_TYPE.BlowTorch: "Blowtorch",
	TOOLS_TYPE.CrossKey: "CrossKey",
}
