extends PanelContainer

signal layer_repaired
export var CAR_ID = 0
export var PART_ID = 0
var STAGE = 0

var tire_texture = preload("res://assets/Icons/Wheel.png")
var engine_texture = preload("res://assets/Icons/Engine.png")
var exhaust_texture = preload("res://assets/Icons/DigestifTube.png")

var blowtorch_texture = preload("res://assets/Icons/Blowtorch.png")
var wrench_texture = preload("res://assets/Icons/EnglishKey.png")
var hammer_texture = preload("res://assets/Icons/Hammer.png")
var crick_texture = preload("res://assets/Icons/Krick.png")
var screwdriver_texture = preload("res://assets/Icons/Screwdriver.png")
var wheelbrace_texture = preload("res://assets/Icons/WheelBrace.png")

var stages = {
	0: {
		0: crick_texture,
		1: wheelbrace_texture,
	},
	1: {
		0: crick_texture,
		1: wheelbrace_texture,
	},
	2: {
		0: crick_texture,
		1: wheelbrace_texture,
	},
	3: {
		0: crick_texture,
		1: wheelbrace_texture,
	},
	4: {
		0: wrench_texture,
		1: hammer_texture,
		2: screwdriver_texture,
	},
	5: {
		0: blowtorch_texture,
	}
}

var labels = {
	0: "Front left wheel",
	1: "Front right wheel",
	2: "Rear left wheel",
	3: "Rear right wheel",
	4: "Engine",
	5: "Exhaust",
}

func _ready():
	# Set the label
	$MarginContainer/VBoxContainer/Label.text = labels[PART_ID]
	
	# Part Icon
	if PART_ID >= 0 and PART_ID <= 3:
		$MarginContainer/VBoxContainer/Icons/Part.texture = tire_texture
	elif PART_ID == 4:
		$MarginContainer/VBoxContainer/Icons/Part.texture = engine_texture
	else:
		$MarginContainer/VBoxContainer/Icons/Part.texture = exhaust_texture
		
	# Tool Icon
	$MarginContainer/VBoxContainer/Icons/Tool.texture = stages[PART_ID][STAGE]

func _on_progress_repair_ui(car_id, part_id, stage, percentage):
	if car_id == CAR_ID and part_id == PART_ID:
		if (percentage >= 100):
			emit_signal("layer_repaired")
			if (stage >= len(stages[PART_ID]) - 1):
				visible = false
			else:
				STAGE = stage + 1
				$MarginContainer/VBoxContainer/ProgressBar.value = 0
				$MarginContainer/VBoxContainer/Icons/Tool.texture = stages[PART_ID][STAGE]
		else:
			$MarginContainer/VBoxContainer/ProgressBar.value = percentage
