extends Panel

export var PLAYER_ID = 0

signal tool_selected
signal tool_changed

var _last_moved = OS.get_ticks_msec()

const input_map = {
	0: {
		"game_left": "p1_game_left",
		"game_right": "p1_game_right",
		"game_item_use": "p1_game_item_use",
	},
	1: {
		"game_left": "p2_game_left",
		"game_right": "p2_game_right",
		"game_item_use": "p2_game_item_use",
	}
}

var items = {
	0: STATIC_TYPES.TOOLS_TYPE.BlowTorch,
	1: STATIC_TYPES.TOOLS_TYPE.Wrench,
	2: STATIC_TYPES.TOOLS_TYPE.Hammer,
	3: STATIC_TYPES.TOOLS_TYPE.Crick,
	4: STATIC_TYPES.TOOLS_TYPE.ScrewDriver,
	5: STATIC_TYPES.TOOLS_TYPE.CrossKey,
}

var item_icon_highlight = {
	STATIC_TYPES.TOOLS_TYPE.BlowTorch: "MarginContainer/VBoxContainer/HBoxContainer/Blowtorch/Selected",
	STATIC_TYPES.TOOLS_TYPE.Wrench: "MarginContainer/VBoxContainer/HBoxContainer/EnglishKey/Selected",
	STATIC_TYPES.TOOLS_TYPE.Hammer: "MarginContainer/VBoxContainer/HBoxContainer/Hammer/Selected",
	STATIC_TYPES.TOOLS_TYPE.Crick: "MarginContainer/VBoxContainer/HBoxContainer/Krick/Selected",
	STATIC_TYPES.TOOLS_TYPE.ScrewDriver: "MarginContainer/VBoxContainer/HBoxContainer/Screwdriver/Selected",
	STATIC_TYPES.TOOLS_TYPE.CrossKey: "MarginContainer/VBoxContainer/HBoxContainer/WheelBrace/Selected",
}

var current_item = 0
var previous_item = 0

func _ready():
	pass
	
func _input(InputEvent):
	if self.visible:
		if _can_move() and is_input_pressed("game_left") and not is_input_pressed("game_right"):
			if current_item <= 0:
				current_item = len(items) - 1
			else:
				current_item -= 1
			
			highlight_item()
			_last_moved = OS.get_ticks_msec()
		elif _can_move() and is_input_pressed("game_right") and not is_input_pressed("game_left"):
			if current_item >= len(items) - 1:
				current_item = 0
			else:
				current_item += 1
			
			highlight_item()
			_last_moved = OS.get_ticks_msec()
		elif is_input_pressed_once("game_item_use"):
			emit_signal("tool_selected", PLAYER_ID, items[current_item])

func highlight_item():
	emit_signal("tool_changed")
	get_node(item_icon_highlight[items[previous_item]]).visible = false
	get_node(item_icon_highlight[items[current_item]]).visible = true
	$MarginContainer/VBoxContainer/Label.text = STATIC_TYPES.get_tool_name(items[current_item])
	previous_item = current_item

func is_input_pressed(input):
	return Input.is_action_pressed(input_map[PLAYER_ID][input])
	
func is_input_pressed_once(input):
	return Input.is_action_just_pressed(input_map[PLAYER_ID][input])
	
func _can_move():
	return OS.get_ticks_msec() - _last_moved > 100
