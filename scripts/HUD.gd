extends Control

signal progress_repair_ui

var total_parts = {
	0: 12,
	1: 12,
}

var current_parts = {
	0: 0,
	1: 0,
}

func _on_Level_update_timer(time):
	var seconds = time % 60
	var mins = int(time / 60)
	$TopBar/Timer/Label.text = "%d:%02d" % [mins, seconds]

func _on_Car_repair(car_id, part_id, stage, percentage):
	if percentage >= 100:
		current_parts[car_id] += 1
			
	var global_percentage = (current_parts[car_id] * 100) / total_parts[car_id]
	if car_id == 0:
		$TopBar/PlayerOneCar/CarStatusOne/ProgressBar.value = global_percentage 
	else:
		$TopBar/PlayerTwoCar/CarStatusTwo/ProgressBar.value = global_percentage
	
	emit_signal("progress_repair_ui", car_id, part_id, stage, percentage)
