extends PanelContainer

signal restart

func _ready():
	pass

func _on_Restart_pressed():
	emit_signal("restart")

func _on_Quit_pressed():
	get_tree().quit()

func _on_Level_end(isWin, time):
	var seconds = time % 60
	var mins = int(time / 60)
	
	$Center/Elements/Lose.visible = not isWin
	$Center/Elements/Win.visible = isWin

	if isWin:
		$Center/Elements/Win/Time.text = "Good job, you took %d:%02d minutes to finish the game!" % [mins, seconds]
