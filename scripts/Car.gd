extends KinematicBody

signal everything_repaired
signal on_car_repair

export var car_id = 0

var _car_parts = []
var _repaired_car_parts = []

func _ready():
	__prep_car_parts()

func __prep_car_parts():
	for child in get_children():
		if child.is_in_group("car_parts"):
			_car_parts.append(child)
			child.connect("repair_finished",
				self, "_on_part_repair_finished", [child])

func _on_part_repair_finished(part):
	_repaired_car_parts.append(part)
	print("Finished repairing: " + str(_repaired_car_parts))
	if len(_repaired_car_parts) == len(_car_parts):
		emit_signal("everything_repaired")
		print("Everything repaired")

func _on_repair(part_id, stage, completion):
	emit_signal("on_car_repair", car_id, part_id, stage, completion)
