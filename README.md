# 🔨 Overfixed 🔧 GGJ 2020 🎮
![Overfixed banner](https://i.imgur.com/idRSyEA.png)

You and your neighbour have to fix your cars before the time runs out. Collaboration is key to make it on time.

[Overfixed on GGJ.org](https://globalgamejam.org/2020/games/overfixed-2)

**Note:** This is a game jam game (Gobal Game Jam 2020), it was made in 48h. The quality of the sources (scenes, code, assets, ...) reflect the time we spent doing this. Some things are smart, but most things are aweful and inconsistent. But the game works and we are happy with the result. Feel free to take inspiration and code snippets on how certain things were done but make sure you understand and verify it before using it. Again, this was done in little time and shortcuts were taken.

_[Made with Godot ](https://godotengine.org/)_ ❤️

## 📷 Screenshots
### In-Game
![In-game screenshot](https://i.imgur.com/RqBW8Ay.png)

### Editor
![Editor screenshot](https://i.imgur.com/YuOYxvv.png)

## 💻 How to install
- Download from [WeTransfer](https://we.tl/t-du71FFFUUa).
- Unzip `Overfixed.zip` somewhere.
- Run `Overfixed.exe`.

## 🕹️ How to play
Players have to grab tools in their toolbox using the interaction key and repair the cars before the time runs out.

### Key maps
You can use a single keyboard or play with two controllers (right analog stick for movment, A to use, B to drop).

#### Player 1
- Forward: W
- Backward: S
- Left: A
- Right: D
- Use: E
- Drop: Q

#### Player 2
- Forward: Up
- Backward: Down
- Left: Left
- Right: Right
- Use: Ctrl
- Drop: Shift

## 🏆 Credits
### Team
- Cristian Gurra
- Damien Toutte
- Dorian Bucur
- Rafael Keramidas
- Salvatore Gambino

### Assets
- Sounds: zapsplat.com
- Sprites : ya-webdesign.com
- Soundtrack : "Le Grand Chase Kevin MacLeod (incompetech.com) Licensed under Creative Commons: By Attribution 3.0 License"